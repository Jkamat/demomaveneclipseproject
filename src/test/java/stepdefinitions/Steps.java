package stepdefinitions;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dataProvider.ConfigFileReader;
import managers.FileReaderManager;
import managers.PageObjectManager;
import pageObjects.HomePage;

public class Steps {
	WebDriver driver;
	HomePage homePage;
    PageObjectManager pageObjectManager;
    ConfigFileReader configFileReader;
	
	@Given("^that I have gone to the Google page$")
	public void that_I_have_gone_to_the_Google_page() throws Throwable {
		System.setProperty("webdriver.chrome.driver", FileReaderManager.getInstance().getConfigReader().getDriverPath());
		driver = new ChromeDriver(); 
	    driver.manage().window().maximize();
	    driver.manage().timeouts().implicitlyWait(FileReaderManager.getInstance().getConfigReader().getImplicitlyWait(), TimeUnit.SECONDS);
		pageObjectManager = new PageObjectManager(driver);
		homePage = pageObjectManager.getHomePage();
        homePage.visitGoogle(driver); }


	@When("^I add \"([^\"]*)\" to the search box$")
	public void i_add_to_the_search_box(String arg1) throws Throwable {
		homePage = new HomePage(driver);	
		homePage.searchGoogle(arg1);
		/*
		 * driver.findElement(By.name("q")).sendKeys(arg1);
		 * driver.findElement(By.name("q")).sendKeys(Keys.ENTER);
		 */
	  
	}

	@When("^click the Search Button$")
	public void click_the_Search_Button() throws Throwable {
		Thread.sleep(2000);
        homePage.clickRemindMe();
			
	  
	}

	@Then("^\"([^\"]*)\" should be mentioned in the results$")
	public void should_be_mentioned_in_the_results(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	 System.out.println("hello"+arg1);
	 driver.close();
	 driver.quit();
	}


	/*
	 * @Given("^user is on home page$") public void user_is_on_Home_Page() throws
	 * Throwable{ System.setProperty("webdriver.chrome.driver",
	 * "C:\\Users\\Janaki.Kamat\\driver\\chromedriver.exe"); driver = new
	 * ChromeDriver(); driver.manage().window().maximize();
	 * driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 * driver.get("https://www.shop.demoqa.com"); }
	 * 
	 * @When("^he search for \"([^\"]*)\"$") public void he_search_for(String arg1)
	 * { driver.navigate().to("https://shop.demoqa.com/?s=" + arg1 +
	 * "&post_type=product"); }
	 * 
	 * @When("^choose to buy the first item$") public void
	 * choose_to_buy_the_first_item() { List<WebElement> items =
	 * driver.findElements(By.cssSelector(".noo-product-inner"));
	 * items.get(0).click();
	 * 
	 * WebElement addToCart =
	 * driver.findElement(By.cssSelector("button.single_add_to_cart_button"));
	 * addToCart.click(); }
	 * 
	 * @When("^moves to checkout from mini cart$") public void
	 * moves_to_checkout_from_mini_cart(){ WebElement cart =
	 * driver.findElement(By.cssSelector(".cart-button")); cart.click();
	 * 
	 * WebElement continueToCheckout =
	 * driver.findElement(By.cssSelector(".checkout-button.alt"));
	 * continueToCheckout.click(); }
	 * 
	 * @When("^enter personal details on checkout page$") public void
	 * enter_personal_details_on_checkout_page() throws Throwable {
	 * Thread.sleep(5000); WebElement firstName =
	 * driver.findElement(By.cssSelector("#billing_first_name"));
	 * firstName.sendKeys("Lakshay");
	 * 
	 * WebElement lastName =
	 * driver.findElement(By.cssSelector("#billing_last_name"));
	 * lastName.sendKeys("Sharma");
	 * 
	 * WebElement emailAddress =
	 * driver.findElement(By.cssSelector("#billing_email"));
	 * emailAddress.sendKeys("test@gmail.com");
	 * 
	 * WebElement phone = driver.findElement(By.cssSelector("#billing_phone"));
	 * phone.sendKeys("07438862327");
	 * 
	 * WebElement countryDropDown =
	 * driver.findElement(By.cssSelector("#billing_country_field .select2-arrow"));
	 * countryDropDown.click(); Thread.sleep(2000);
	 * 
	 * List<WebElement> countryList =
	 * driver.findElements(By.cssSelector("#select2-drop ul li")); for(WebElement
	 * country : countryList){ if(country.getText().equals("India")) {
	 * country.click(); Thread.sleep(3000); break; } }
	 * 
	 * WebElement city = driver.findElement(By.cssSelector("#billing_city"));
	 * city.sendKeys("Delhi");
	 * 
	 * WebElement address =
	 * driver.findElement(By.cssSelector("#billing_address_1"));
	 * address.sendKeys("Shalimar Bagh");
	 * 
	 * WebElement postcode =
	 * driver.findElement(By.cssSelector("#billing_postcode"));
	 * postcode.sendKeys("110088");
	 * 
	 * WebElement countyDropDown =
	 * driver.findElement(By.cssSelector("#billing_state_field .select2-arrow"));
	 * countyDropDown.click(); Thread.sleep(2000);
	 * 
	 * List<WebElement> countyList =
	 * driver.findElements(By.cssSelector("#select2-drop ul li")); for(WebElement
	 * county : countyList){ if(county.getText().equals("Delhi")) { county.click();
	 * Thread.sleep(3000); break; } } }
	 * 
	 * @When("^select same delivery address$") public void
	 * select_same_delivery_address() throws InterruptedException{ WebElement
	 * shipToDifferetAddress =
	 * driver.findElement(By.cssSelector("#ship-to-different-address-checkbox"));
	 * shipToDifferetAddress.click(); Thread.sleep(3000); }
	 * 
	 * @When("^select payment method as \"([^\"]*)\" payment$") public void
	 * select_payment_method_as_payment(String arg1){ List<WebElement> paymentMethod
	 * = driver.findElements(By.cssSelector("ul.wc_payment_methods li"));
	 * paymentMethod.get(0).click(); }
	 * 
	 * @When("^place the order$") public void place_the_order() { WebElement
	 * acceptTC = driver.findElement(By.cssSelector("#terms.input-checkbox"));
	 * acceptTC.click();
	 * 
	 * WebElement placeOrder = driver.findElement(By.cssSelector("#place_order"));
	 * placeOrder.submit(); }
	 */

}