package pageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import managers.FileReaderManager;



public class HomePage {
	WebDriver driver;
	public HomePage(WebDriver driver) {
	     PageFactory.initElements(driver, this);
	 }
	 @FindBy(how = How.NAME, using = "q") 
	 private WebElement search;
	 
	 @FindBy(how = How.XPATH, using = "//*[@id=\"cnsd\"]") 
	 private WebElement reminMeClick;
	 
	 public void visitGoogle(WebDriver driver) {
		 driver.get(FileReaderManager.getInstance().getConfigReader().getApplicationUrl()); 
	 }
	 public void searchGoogle(String value) {
		 search.sendKeys(value);
		 search.sendKeys(Keys.ENTER);
	 }
	 
	 public void clickRemindMe() {
		 reminMeClick.click();
	 }
	
}
